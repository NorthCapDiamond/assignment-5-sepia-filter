#include "bmp.h"
#include "sepia.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

extern void sepia_filter_asm(void*, void*, uint32_t, uint32_t);

int main(int argc, char** argv){
    if(argc==3){
        int file_descriptor_input = open(argv[1], 0);
        int file_descriptor_output = open(argv[2], 0x0042, 0x1b6);

        copy_img_hdr(file_descriptor_input, file_descriptor_output);


        struct image* img_input = bmp_to_img(file_descriptor_input);
	    struct image* img_output = bmp_to_img(file_descriptor_output);

        sepia(img_input, img_output);

        img_to_bmp(file_descriptor_output, img_output);

         
        return 0;

    }
    else{
        return 1;
    }

}



