main: main.o bmp.o sepia.o sepia.asm asm.o
	gcc -O3 -g -F dwarf main.o bmp.o sepia.o asm.o -no-pie -o main

main.o: main.c bmp.h sepia.h
	gcc -g -F dwarf main.c -no-pie -c

bmp.o: bmp.c bmp.h
	gcc -O3 bmp.c -no-pie -c

sepia.o: sepia.c sepia.h
	gcc  -O3 sepia.c -no-pie -c

asm.o: sepia.asm
	nasm -f elf64 -g -F dwarf sepia.asm -o asm.o

 