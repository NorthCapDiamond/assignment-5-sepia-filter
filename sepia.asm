global sepia_filter_asm

section .data

align 16
color_red_coefficients dd 0.393, 0.349, 0.272, 0.0   
align 16
color_green_coefficients dd 0.769, 0.686, 0.534, 0.0 
align 16
color_blue_coefficients dd 0.189, 0.168, 0.131, 0.0   
align 16
color_max_values dd 255, 255, 255, 0

 

section .text


; Тут я буду минимизировать затраты на mul
; если организовать векторное умножение по следующему принципу, то можно за 1 действие проводить 3 полезных операции за раз ( 4я: 0*0)
; (from xmm0:  || Red || Red || Red || 0 || ) * (from xmm3: || Red_coefficients || Red_coefficients || Red_coefficients || 0 || ) -> xmm0
; (from xmm1:  || Green || Green || Green || 0 || ) * (from xmm4: || Green_coefficients || Green_coefficients || Green_coefficients || 0 || ) -> xmm1
; (from xmm2:  || Blue || Blue || Blue || 0 || ) * (from xmm5: || Blue_coefficients || Blue_coefficients || Blue_coefficients || 0 || )  ->xmm2


; => Далее суммируем эти 3 регистра: xmm0 = xmm0 + xmm1 + xmm2, но при этом нужно делать проверку на значение: 
; если значение параметра (|| R || G || B ||) > color_max_values (255, 255, 255), тогда нужно сделать его равным 255; 
; таким образом мы и выигрываем в производитеьности, тк операция умножения занимает 3-4 такта: https://uops.info/table.html

align 8
sepia_filter_asm:
	movups  xmm3, [color_red_coefficients]  
	movups  xmm4, [color_green_coefficients]  
	movups xmm5, [color_blue_coefficients]  
	movups xmm6, [color_max_values]         
	mov r8d, edx 	; width 
	pxor xmm0, xmm0
	nop 			; here i did the alignment
    
	
	.loop:
		lodsb 
		pinsrb xmm0, al, 0  
		pinsrb xmm0, al, 4   
		pinsrb xmm0, al, 8   
		cvtdq2ps xmm0, xmm0
		mulps xmm0, xmm3
		pxor xmm1, xmm1
		lodsb 
		pinsrb xmm1, al, 0
		pinsrb xmm1, al, 4
		pinsrb xmm1, al, 8		
		cvtdq2ps xmm1, xmm1
		mulps xmm1, xmm4
		pxor xmm2, xmm2
		lodsb 
		pinsrb xmm2, al, 0
		pinsrb xmm2, al, 4
		pinsrb xmm2, al, 8
		cvtdq2ps xmm2, xmm2
		mulps xmm2, xmm5

		addps xmm0, xmm1
		addps xmm0, xmm2

		cvtps2dq xmm0, xmm0
		pminud xmm0, xmm6

		pextrb byte [rdi], xmm0, 0
		pextrb byte [rdi+1], xmm0, 4
		pextrb byte [rdi+2], xmm0, 8

		add rdi, 3
		dec r8d

	jnz .loop

		mov r8d, edx
		dec ecx

	jnz .loop
ret
