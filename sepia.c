#include "sepia.h"
#include "bmp.h"
#include <stdint.h>
#include <stdio.h>

void sepia(struct image* image_input*, struct image* image_output){
    float color_R;
    float color_G;
    float color_B;
 
    struct pixel* curr_input = image_input->pixel_data; 
    struct pixel* curr_output = image_output->pixel_data;

    const uint32_t tmp_width = image_input->width;
    const uint32_t tmp_height = image_input->height;

    for(uint32_t i = 0; i < tmp_width; i++){
        for(uint32_t j=0; j < tmp_width; j++){
            color_R = (curr_input)->R *0.393f  + (curr_input)->G *0.769f + (curr_input)->B *0.189f ;
		    color_G = (curr_input)->R *0.349f  + (curr_input)->G *0.686f + (curr_input)->B *0.168f ;
		    color_B = (curr_input)->R *0.272f  + (curr_input)->G *0.534f + (curr_input)->B *0.131f ;


            if(color_R > 255.0f){
                (curr_output)->R = 255;
            }
            else{
                (curr_output)->R = (uint8_t)color_R;
            }
            if(color_G > 255.0f){
                (curr_output)->G = 255;
            }else{
                (curr_output)->G = (uint8_t)color_G;
            }
            if(color_B > 255.0f){
                (curr_output)->B = 255;
            }else{
                (curr_output)->B = (uint8_t)color_B;
            }
        }
        curr_input++;
        curr_output++;
    }
}
