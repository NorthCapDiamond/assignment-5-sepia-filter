#include "image.h"
#include "bmp.h"
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>


struct image* bmp_to_img(FILE* bmp_handle){
	struct bmp_header bmp_hdr;
	if(fread(&bmp_hdr, 1, sizeof(struct bmp_header), bmp_handle) != sizeof(struct bmp_header)){
		/* add error handling */ 
	}

	/* add bmp file format check if() */ 

	struct image* img = (struct image*)malloc(sizeof(struct image));
	img->width = bmp_hdr.biWidth;
	img->height = bmp_hdr.biHeight;

	uint32_t padbytes = 0;
	if((img->width*3)%4){
		padbytes = 4 - (img->width*3)%4;
	}

	if(sizeof(struct bmp_header) != bmp_hdr.bOffBits){
		fseek(bmp_handle, bmp_hdr.bOffBits - sizeof(struct bmp_header), SEEK_CUR);
	}

	img->pixel_data = (struct pixel*)malloc(img->width*img->height*3);

	uint32_t pixoffs = 0;

	for(uint32_t i = 0; i < img->height; i++){
		pixoffs += fread((char*)img->pixel_data+pixoffs, 1, img->width*3, bmp_handle);
		if(padbytes){
			fseek(bmp_handle, padbytes, SEEK_CUR);
		}
	}

	fseek(bmp_handle, 0, SEEK_SET);

	return img;
}

void img_to_bmp(struct image* img, FILE* file){
	fseek(file, 0, SEEK_SET);
	struct bmp_header bmp_hdr = {0};
	fread(&bmp_hdr, sizeof(struct bmp_header), 1, file);
	fseek(file, bmp_hdr.bOffBits, SEEK_SET);

	uint32_t padbytes = 0;
	if((bmp_hdr.biWidth*3) % 4){
		padbytes = 4 - (bmp_hdr.biWidth*3) % 4;
	}
	for(int i = 0; i < bmp_hdr.biHeight; i++){
		fwrite(((char*)img->pixel_data)+(i*img->width*3), img->width*3, 1, file);
		if(padbytes){
			fseek(file, padbytes, SEEK_CUR);
		}
		printf("%d\n", i);
	}

}

void copy_img_hdr(FILE *file_1, FILE *file_2){
	char buff[14];
	fread(buff,14,1,file_2);
	

	fflush(file_2);
	ftruncate(file_2->_fileno, *((uint32_t*)(buff+2)));


	void *tmp_buff = malloc(*((uint32_t*)(buff+0xA)));
	fseek(file_1, 0, SEEK_SET);
	fread(tmp_buff, *((uint32_t*)(buff+0xA)), 1, file_1);
	fwrite(tmp_buff, 1, *((uint32_t*)(buff+0xA)), file_2);
	fseek(file_1, 0, SEEK_SET);
	fseek(file_2, 0, SEEK_SET);

}
