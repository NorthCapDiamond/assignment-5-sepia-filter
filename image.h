#ifndef __IMAGE_H__
#define __IMAGE_H__


#include "stdint.h"

struct pixel{
	uint8_t R;
	uint8_t G;
	uint8_t B;

}__attribute__((packed));

struct image{
	uint32_t width;
	uint32_t height;

	struct pixel* pixel_data;
};
 
#endif
