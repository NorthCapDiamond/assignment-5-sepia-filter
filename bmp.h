#ifndef __BMP_H__
#define __BMP_H__

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved; 
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
}__attribute__((packed)); 


struct image* bmp_to_img(FILE* bmp_handle);


void img_to_bmp(struct image* img, FILE* file);


struct image* creat_bmp_and_img_accordinglyto_img(FILE* filei, FILE* fileo);
void copy_img_hdr(FILE *file_1, FILE *file_2);


#endif
